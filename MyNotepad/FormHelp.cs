﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyNotepad
{
    public partial class FormHelp : Form
    {
        public FormHelp()
        {
            InitializeComponent();
        }

        private void FormHelp_Load(object sender, EventArgs e)
        {
            label2.Text = "My Notepad — свободный текстовый редактор.\n\nНаписан на C# группой гениальных программистов. Поддерживает открытие и сохранение файлов формата .txt.";
            label3.Text = "Недостатки: \n-Ебаная прокрутка \n-Не выделяет синтаксис.";
        }
    }
}
