﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace MyNotepad
{
    public partial class FormMain : Form
    {
        /*private const int SB_HORZ = 0x0;
        private const int SB_VERT = 0x1;
        private const int WM_HSCROLL = 0x114;
        private const int WM_VSCROLL = 0x115;
        private const int SB_THUMBPOSITION = 4;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int GetScrollPos(int hWnd, int nBar);

        [DllImport("user32.dll")]
        private static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

        [DllImport("user32.dll")]
        private static extern bool PostMessageA(IntPtr hWnd, int nBar, int wParam, int lParam);

        internal int HScrollPos
        {
            private get { return GetScrollPos((int)this.Handle, SB_HORZ); }
            set
            {
                SetScrollPos((IntPtr)this.Handle, SB_HORZ, value, true);
                PostMessageA((IntPtr)this.Handle, WM_HSCROLL, SB_THUMBPOSITION + 0x10000 * value, 0);
            }
        }
        internal int VScrollPos
        {
            private get { return GetScrollPos((int)this.Handle, SB_VERT); }
            set
            {
                SetScrollPos((IntPtr)this.Handle, SB_VERT, value, true);
                PostMessageA((IntPtr)this.Handle, WM_VSCROLL, SB_THUMBPOSITION + 0x10000 * value, 0);
            }
        }
        */



        private bool isSave;
        private string savePath;

        private void PrintCountTextLines()
        {
            if (richTextBoxText.Text.Length == 0)
            {
                richTextBoxNumbers.Text = "1";
            }
            else
            {
                richTextBoxNumbers.Clear();
                for (int i = 0; i < richTextBoxText.Lines.Length; i++)
                {
                    richTextBoxNumbers.Text += (i + 1) + "\n";
                }
            }
            //richTextBoxNumbers.SelectionStart = richTextBoxNumbers.Text.Length;
            //richTextBoxNumbers.ScrollToCaret();
        }

        public FormMain()
        {
            InitializeComponent();
        }

        private void новыйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isSave = false;
            savePath = string.Empty;

            richTextBoxNumbers.Text = "1";

            toolStripStatusLabelFile.Text = "Файл не сохранен!";
            toolStripStatusLabelTextLenght.Text = "Длина текста: 0";
            toolStripStatusLabelLines.Text = "Линий тескста: 1";
            toolStripStatusLabelCurrentPosirion.Text = "Строка: 1, столбец: 1";

            richTextBoxText.Clear();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            новыйToolStripMenuItem_Click(null, null);
        }

        private void отменитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBoxText.Undo();
        }

        private void вырезатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBoxText.Cut();
        }

        private void копироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBoxText.Copy();
        }

        private void вставитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBoxText.Paste();
        }

        private void выделитьВсеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBoxText.SelectAll();
        }

        private void времяИДатаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBoxText.Text += DateTime.Now;
            richTextBoxText.Select(richTextBoxText.Text.Length, 0);
        }

        private void помощьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormHelp().ShowDialog();
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormAbout().ShowDialog();
        }

        private void шрифтToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fontDialog.ShowDialog() == DialogResult.OK)
            {
                richTextBoxText.Font = fontDialog.Font;
                richTextBoxNumbers.Font = fontDialog.Font;
            }
        }

        private void цветШрифтаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                richTextBoxText.ForeColor = colorDialog.Color;
            }
        }

        private void выравниваниеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            поЛевомуКраюToolStripMenuItem_Click(null, null);
        }

        private void поЛевомуКраюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBoxText.SelectAll();

            richTextBoxText.SelectionAlignment = HorizontalAlignment.Left;
            richTextBoxText.Select(richTextBoxText.Text.Length, 0);

        }

        private void поПравомуКраюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBoxText.SelectAll();

            richTextBoxText.SelectionAlignment = HorizontalAlignment.Right;
            richTextBoxText.Select(richTextBoxText.Text.Length, 0);
        }

        private void поЦентруToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBoxText.SelectAll();

            richTextBoxText.SelectionAlignment = HorizontalAlignment.Center;
            richTextBoxText.Select(richTextBoxText.Text.Length, 0);
        }

        private void richTextBoxText_TextChanged(object sender, EventArgs e)
        {
            toolStripStatusLabelTextLenght.Text = $"Длина текста: {richTextBoxText.Text.Length}";
            toolStripStatusLabelLines.Text = $"Линий тескста: {richTextBoxText.Lines.Length}";

            PrintCountTextLines();


            //int row = richTextBoxText.GetLineFromCharIndex(richTextBoxText.SelectionStart) + 1;
            //int column = richTextBoxText.SelectionStart - richTextBoxText.GetFirstCharIndexOfCurrentLine() + 1;

            //toolStripStatusLabelCurrentPosirion.Text = $"Строка: {row}, столбец: {column}";
        }

        private void richTextBoxText_VScroll(object sender, EventArgs e)
        {
            //richTextBoxText.            
        }

        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                isSave = true;
                savePath = saveFileDialog.FileName;
                toolStripStatusLabelFile.Text = savePath;
                richTextBoxText.SaveFile(savePath, RichTextBoxStreamType.UnicodePlainText);

                MessageBox.Show("Файл сохранен!");
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isSave == true)
            {
                richTextBoxText.SaveFile(savePath, RichTextBoxStreamType.UnicodePlainText);

                MessageBox.Show("Файл сохранен!");
            }
            else
            {
                сохранитьКакToolStripMenuItem_Click(null, null);
            }
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isSave == false)
            {
                if (MessageBox.Show("Сохранить файл?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    сохранитьКакToolStripMenuItem_Click(null, null);
                }
            }
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(openFileDialog.ShowDialog() == DialogResult.OK)
            {
                isSave = false;
                savePath = string.Empty;

                toolStripStatusLabelFile.Text = "Файл не сохранен!";

                richTextBoxText.Text = File.ReadAllText(openFileDialog.FileName,Encoding.Unicode);
            }
        }

        private void richTextBoxText_KeyDown(object sender, KeyEventArgs e)
        {
            int row = richTextBoxText.GetLineFromCharIndex(richTextBoxText.SelectionStart) + 1;
            int column = richTextBoxText.SelectionStart - richTextBoxText.GetFirstCharIndexOfCurrentLine() + 1;

            toolStripStatusLabelCurrentPosirion.Text = $"Строка: {row}, столбец: {column}";
        }

        private void richTextBoxText_MouseDown(object sender, MouseEventArgs e)
        {
            richTextBoxText_KeyDown(null, null);
        }
    }
}
