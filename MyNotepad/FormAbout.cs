﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyNotepad
{
    public partial class FormAbout : Form
    {
        public FormAbout()
        {
            InitializeComponent();
        }

        private void FormAbout_Load(object sender, EventArgs e)
        {
            label2.Text = "Версия программы 1.02 \n \n ©Корпорация What is love programmers (What is love Corporation), 2018. Все права защищены. \n \n Продук лицензирован, но я вам об этом не скажу =).";
        }
    }
}
